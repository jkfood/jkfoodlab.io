---
title: "Slow Cooker Buffalo Chicken"
date: 2020-12-24T14:31:21-08:00
categories:
- Entrees
tags:
- chicken breast
- Hot Wing Sauce
---

## Ingredients 

- 18 oz raw boneless, skinless chicken breasts (yields 12 oz cooked)
- 1/4 cup Frank's Hot Wing Sauce 
- 2 tbsp butter, melted 

## Directions 

Combine hot wing sauce and margarine in a slow cooker. Add chicken breasts and toss to coat in sauce. Cook on low for 6 to 8 hours. Shred chicken in the pot (optional) and serve over salad