---
title: "One Pot Andouille Sausage Skillet Pasta"
date: 2020-12-22T14:43:31-08:00
categories:
- Entrees
- Pasta
tags:
- zucchini
- squash
- bell peppers
- andouille sausage
- pasta
- pepper jack cheese
---

## Ingredients 

- 1 tablespoon olive oil
- 2 cloves garlic, minced
- 1/2  onion, diced
- 1-2 cups diced zucchini/squash/bell peppers
- 1 (12.8-ounce) package smoked andouille sausage, thinly sliced
- 1 (14.5-ounce) can diced tomatoes
- 1/2 cup milk
- 8 ounces elbows pasta
- ¾ tsp Cajun seasoning
- Kosher salt and freshly ground black pepper, to taste
- 1 cup shredded pepper jack cheese//

## Directions 

1. Heat olive oil in a large skillet over medium high heat. Add vegetables and sausage, and cook, stirring frequently, until sausage is lightly browned, about 3-4 minutes.
1. Stir in chicken brown, tomatoes, milk and pasta; season with salt and pepper, to taste. Bring to a boil; cover, reduce heat and simmer until pasta is cooked through, about 12-14 minutes.
1. Remove from heat and top with cheese. Cover until cheese has melted, about 2 minutes.
1. Serve immediately.
