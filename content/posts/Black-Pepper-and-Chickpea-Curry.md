---
title: "Black Pepper and Chickpea Curry"
date: 2020-12-22T21:09:05-08:00
categories:
- Entrees
tags:
- cauliflower
- chickpeas
---

4 servings

## Ingredients 

- 1 medium yellow onion, halved and thinly sliced lengthwise
- 2 tsp. minced garlic
- 1/2 medium cauliflower, chopped into small florets about the size of a grape
- 2 teaspoons freshly ground black pepper
- 1 Tbs. minced fresh ginger
- 2 tsp. hot curry powder, such as Madras
- 1 ½ tsp. cumin
- salt
- 1 can diced tomatoes
- 1 can chick peas NOT drained
- 3 handfuls [baby or chopped] spinach
- 3 Tbs. chopped fresh cilantro (optional)
- 4-6 tablespoons natural yoghurt (plain)

## Directions 

1. Heat a little oil in a medium saucepan. Cook onion and garlic covered over a medium low heat until soft, about 8 minutes.
2. Add cauliflower, and spices. Stir to combine.
3. Add  tomatoes and chickpeas and the juices from both cans. Use a spoon to break up the tomatoes a little.
4. Cover and simmer for 10 minutes on a medium heat. 
5. Add spinach, cover and simmer for another 5 minutes.
6. Stir and serve with a dollop of plain yogurt
