---
title: "Simple and Meaty Skillet Lasagna"
date: 2020-12-24T14:49:15-08:00
categories:
- Entrees
- Pasta
tags:
- lasagna noodles
- italian sausage
- mozzarella cheese
- ricotta cheese
- parmesan cheese
- spinach
---

## Ingredients 

- 2-3 (14.5 ounce) cans petite diced tomatoes
- 1 (8 ounce) can tomato sauce
- 1 tablespoon olive oil
- 1 medium onion, minced
- Salt
- 3 medium garlic cloves, finely minced
- 1/2 tablespoon dried basil
- 1 teaspoon dried oregano
- 2 links of hot italian chicken sausage ([version with ground pork and beef](http://www.melskitchencafe.com/simple-and-meaty-skillet-lasagna/))
- 8 oz pasta
- ½-1 cup shredded mozzarella cheese
- 1-3 handfuls spinach
- 1/2 cup grated Parmesan cheese
- 3/4 cup cottage (or ricotta) cheese 


## Directions 

1. Heat the oil in a 12-inch nonstick skillet over medium heat. Add the onion and 1/2 teaspoon salt and cook, stirring often, until the onion is softened and translucent, about 5 to 7 minutes. Stir in the garlic and cook until fragrant, about 30 seconds. Add the ground meat and cook, breaking apart the meat, until it is lightly browned and no longer pink, 5 to 6 minutes.
1. Scatter the noodles over the meat and then pour in the tomatoes and tomato sauce over the pasta. Stir in the basil and oregano. Cover, increase the heat to medium-high, and cook, stirring often and adjusting the heat to maintain a vigorous simmer, until the pasta is tender, about 20 minutes.
1. Add spinach and mix it in.
1. Heat broiler.
1. Off the heat, stir in half the mozzarella and half of the parmesan. Dot heaping tablespoons of the ricotta over the noodles, then sprinkle with the remaining mozzarella and parmesan. Place skillet under broiler until cheeses melt. 
1. Serve.
