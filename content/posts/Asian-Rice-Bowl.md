---
title: "Asian Rice Bowl"
date: 2020-12-16T19:19:28-08:00
categories:
- Entrees
tags:
- chicken breast
- bell peppers
- mushrooms
- broccoli
- carrots
---

## Ingredients 

- 14 oz. raw chicken breast (10.5 oz. cooked)
- 2-2/3 cup cooked rice
- 1 cup chopped bell peppers
- 1 cup sliced mushrooms
- 1 cup chopped broccoli
- 1 cup matchstick carrots
- 2 Tbs. olive oil
- 2 Tbs. coconut aminos (or soy sauce)
- 2 Tbs. honey
- 1 Tbs. fresh lemon juice
- 2 tsp. Worcestershire sauce
- 2 tsp. balsamic vinegar
- 2 tsp. sesame oil
- 1 tsp. dry mustard
- 1⁄2 tsp. black pepper
- Dash of sea salt
- Dash of garlic powder
- Green onions
- 4 tsp. sesame seeds

## Directions 

1. Place olive oil, coconut aminos, honey, lemon juice, Worcestershire sauce, vinegar, sesame oil, dry mustard, black pepper, sea salt and minced garlic in a large Ziploc bag. Shake and massage mixture until combined. Add all chopped veggies to the bag and let marinate in the fridge 2+ hours.
1. Cook and chop chicken. Prepare rice according to the directions on the package.
1. Place veggies in a large skillet over medium high heat. Cook and stir veggies about 5 minutes, or until tender. For one serving, serve 1/3 cup cooked rice, with 2.6 oz. cooked chicken and 1⁄4 of the marinated veggies in a bowl. Top with sliced green onions and 1 tsp. sesame seeds.