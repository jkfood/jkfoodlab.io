---
title: "Overnight Oats"
date: 2020-12-16T19:59:53-08:00
categories:
- Breakfast
tags:
- Oatmeal
---

## Ingredients 

- ⅔ cup yogurt
- 1 heaping cup old fashioned oats
- 1 ⅓ cups milk
- 2 Tbs chia seeds
- 2 Tbs honey
- 1 tsp vanilla
- 2 pinches salt

Doubled:
- 1 ⅓ cup yogurt
- 2 heaping cups oats
- 2 ⅔ cups milk
- ¼ cup chia seeds
- ¼ cup honey
- 2 tsp vanilla
- 4 pinches salt


## Directions 

Combine ingredients and let it set overnight