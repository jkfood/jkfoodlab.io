---
title: "Quick Chicken Vindaloo"
date: 2020-12-22T21:12:44-08:00
categories:
- Entrees
tags:
- chicken thighs
- red wine vinegar
- hot paprika
---

Vindaloo is an extremely spicy Indian curry dish; our version has heat but doesn’t overwhelm. The dish goes well with jasmine rice and naan. If you've got time, make your own Homestyle [Indian Naan](http://www.finecooking.com/recipes/homestyle_indian_naan.aspx), but if you make the vindaloo on a weeknight, store-bought naan is fine.

Serves 4

## Ingredients 

- 1-1/2 Tbs. curry powder 
- 2 tsp. hot paprika 
- Freshly ground black pepper 
- 1-1/2 lb. boneless, skinless chicken thighs, trimmed and cut into 3/4- to 1-inch pieces 
- 8 medium cloves garlic, minced 
- 4 Tbs. red wine vinegar 
- Kosher salt 
- 2 Tbs. canola oil 
- 1 medium yellow onion, thinly sliced 
- 1 Tbs. grated fresh ginger 
- 1 (14-1/2-oz). can diced tomatoes, drained, 1/3 cup juice reserved 
- 3 Tbs. chopped fresh cilantro 

## Directions 

In a small bowl, stir the curry powder, paprika, and 3/4 tsp. black pepper. Put the chicken in a medium nonreactive bowl, sprinkle with 1 Tbs. of the curry powder mixture, about half of the garlic, 2 Tbs. of the vinegar, and 3/4 tsp. salt; toss to coat. Set aside at room temperature.

Heat the oil in a 10- to 11-inch straight-sided sauté pan over high heat until shimmering. Add the onion and ¼ tsp. salt and cook, stirring occasionally with a wooden spoon, until it softens and begins to brown around the edges, 4 to 5 minutes. Reduce the heat to medium high, add the ginger, the remaining garlic, and the remaining curry powder mixture and cook, stirring, until fragrant and well combined, about 45 seconds.

Add the tomatoes and mix to combine, scraping the bottom of the pan with the spoon. Stir in the chicken, reserved tomato juice, remaining 2 Tbs. vinegar, and 2/3 cup water. Bring to a boil, cover partially, reduce the heat to medium, and simmer, stirring occasionally, until the chicken is tender and cooked through, 15 to 20 minutes. Season to taste with salt and pepper. Serve sprinkled with the cilantro.
serving suggestions
Serve with a hearty side like Green Lentils with Green Beans and Cilantro.

nutrition information (per serving):
Calories (kcal): 360; Fat (g): 20; Fat Calories (kcal): 180; Saturated Fat (g): 4; Protein (g): 33; Monounsaturated Fat (g): 9; Carbohydrates (g): 12; Polyunsaturated Fat (g): 5; Sodium (mg): 670; Cholesterol (mg): 110; Fiber (g): 2;

[source](http://www.finecooking.com/recipes/quick-chicken-vindaloo.aspx)