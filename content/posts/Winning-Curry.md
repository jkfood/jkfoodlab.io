---
title: "Winning Curry"
date: 2020-12-22T21:16:13-08:00
categories:
- Entrees
tags:
- red curry paste
---

## Directions 

Heat up 1 Tbs vegetable oil in a medium size pot. Add 2-3 Tbs. red curry paste. Mix until oil is absorbed. Let that cook for a bit?

add 2 cans coconut milk, stir on a simmer until paste is totally dispersed.
Add 1 Tbs. fish sauce
Add 1 Tbs. brn sugar
let simmer. After it simmers a few minutes, taste the base. It should be what you want it to taste like, only more intense.

Add 1 sweet potato cubed 1inchish. Let that cook until it’s mostly done. 
Bring to a boil. Then back down to medium-high. 
Add 1 thinly sliced chicken breast that has been sliced across the grain.
Bring to a boil. Then back down to medium-high. 
After the chicken is mostly cooked, 
add the carrots and the sweet onion. Let that cook. 
After those veggies are cooked, add the bell pepper. Cook 5 more minutes. 
