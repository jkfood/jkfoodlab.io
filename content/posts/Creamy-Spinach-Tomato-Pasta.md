---
title: "Creamy Spinach Tomato Pasta"
date: 2020-12-22T14:49:37-08:00
categories:
- Entrees
- Pasta
tags:
- pasta
- spinach
- heavy cream
- parmesan cheese
---

## Ingredients 

- 1 (12 ounce) package pasta
- 2 TBS minced onion (fresh)
- 2 tsp minced garlic
- 1 (14.5 ounce) can diced tomatoes
- 1 1/2 teaspoons dried basil
- 1/2 teaspoon salt
- 1/4 teaspoon pepper
- 2 cups chopped fresh spinach
- 2 tablespoons all-purpose flour
- 3/4 cup milk
- 3/4 cup heavy cream
- 1/4 cup grated Parmesan cheese

## Directions 


1. Bring a large pot of water to a boil. Add the pasta and a tsp. Of olive oil, and cook until tender, about 10 minutes.
1. While you get the pasta going, saute the onion and garlic.
1. Add the tomatoes, salt, pepper, and basil in a large saucepan over medium heat. Cook and stir until the mixture begins to bubble. Add the spinach, reduce heat to low and cover. 
1. In a sauce pan, stir together the milk, cream, and flour. Add the parmesan cheese. Heat through, then reduce heat to low, and simmer until thick, about 2 minutes.

Pour the cream mixture into the tomato mixture and mix. Add the pasta and stir to coat. 
