---
title: "Meatloaf"
date: 2020-12-22T21:04:11-08:00
categories:
- Entrees
tags:
- ground beef
---

## Ingredients 

Saute:
- 4 cloves garlic, minced
- 1 onion, diced

Mix together:
- 1 egg
- 1 cup rolled oats
- 2 Tbs. brown sugar
- 6 Tbs. ketchup
- 2 Tbs. worcestershire sauce
- 2 tsp. prepared mustard
- 1 ½ lb. ground beef
- ½ tsp salt
- ¼ tsp ground pepper
- 6 shakes oregano

## Directions 

Add onion and garlic to the meat mixture and pour into a loaf pan (or shape on a cookie sheet) that has been lined with foil and sprayed with pam. Bake at 350 degrees for 1 hour. Let the meatloaf cool/”rest” before you cut it. 
