---
title: "Spicy Pumpkin Chipotle Burrata Lasagna"
date: 2020-12-24T14:55:34-08:00
categories:
- Entrees
- Pasta
tags:
- italian sausage
- chipotles in adobo
- heavy cream
- pumpkin puree
- ricotta cheese
- lasagna noodles
- pumpkin seeds
---
Prep time: 20 mins
Cook time: 1 hour
Total time: 1 hour 45 mins
Yield: 12 servings

## Ingredients 

- 1 lb [chicken] Italian sausage, browned
- 1-3 tsp canned chipotles in adobo
- 2 cups heavy cream (I used just 1 cup and it was fine)
- 16 oz pumpkin puree
- 1 Tbsp pumpkin pie spice
- 3 cloves of garlic
- 1 Tbsp olive oil
- 1/2 tsp salt
- 1/2 cup chicken broth
- 15 oz ricotta
- 1 cups shredded Italian cheese
- 1/2 Tbsp Italian Seasoning
- 2 (9 oz) boxes no-cook lasagna noodles (you may not use them all)
- 4 oz burrata (I used mozzarella)
- 1/2 tsp paprika
- 2 Tbs. cup roasted pumpkin seeds

## Directions 

1. Preheat oven to 350°F.
1. Place all sauce ingredients in a blender. Puree. Set aside 1/4 cup.
1. Cook Italian Sausage and mix with sauce.
1. In a large bowl, mix together ricotta, cheese and Italian seasoning.
1. Drizzle a small amount of olive oil and the 1/4 cup of reserved sauce into the bottom of a 9x13 pan. Layer: noodles, meat sauce, noodles, 3/4 of the cheese mixture, noodles, meat sauce, noodles, small amount meat sauce (last time the top noodles never softened because they didn’t have sauce on top?), then spoon dollops of the remaining cheese atop. Cover with foil.
1. Bake for 35 minutes. Then remove pan from the oven. Top with burrata, a drizzle of olive oil, paprika and pumpkin seeds. Return to oven. Bake uncovered for 20-30 minutes more. Or until cheese is melted and slightly toasted.
1. For best results, allow to cool for 15-20 minutes before slicing and serving.

