---
title: "Cauliflower Alfredo Sauce"
date: 2020-12-24T14:33:19-08:00
categories:
- Entrees
- Pasta
tags:
- cauliflower
- parmesan cheese
---

## Ingredients 

- 3 jumbo clove garlic, minced
- 2 TB butter
- Cauliflower florets from 1 head of cauliflower
- 6-7 cups water or vegetable/chicken broth
- 1 tsp salt 
- 1/2 tsp pepper 
- 1/2 cup milk 
- 1 handful Parmesan or Romano cheese

## Directions 

1. Garlic: Saute the minced garlic with the butter in a large nonstick skillet over low heat. Cook for several minutes or until the garlic is soft and fragrant but not browned. Remove from heat and set aside.
2. Cauliflower--2 options: A) Bring the water or vegetable/chicken broth to a boil in a large pot. Add the cauliflower and cook, covered, for 7-10 minutes or until cauliflower is fork tender. Do not drain. OR B) Steam cauliflower until fork tender and use broth in the next step.
3. Puree: Use a slotted spoon to transfer the cauliflower pieces to the blender. Add 1 cup broth, sauteed garlic/butter, salt, pepper, milk, and cheese if desired. Blend or puree for several minutes until the sauce is very smooth, adding more broth or milk depending on how thick you want the sauce. You may have to do this in batches depending on the size of your blender. Serve hot over cooked pasta. 