---
title: "Chicken Teriyaki Kabobs"
date: 2020-12-22T14:37:05-08:00
categories:
- Entrees
tags:
- chicken breast
- bell pepper
- red onion
- zucchini
---

## Ingredients 

- 2 tablespoon cornstarch
- 1/2 cup soy sauce
- 1/2 cup brown sugar, packed
- 1 teaspoon ground ginger
- 1/2 teaspoon garlic powder
- 1 1/2 pounds boneless, skinless, chicken breasts, cut into 1-inch chunks
- Sesame seeds, for garnish
- Sliced green onions, for garnish
- Veggies: bell pepper, (red) onion, zucchini, squash, etc. 
- Serve with rice if desired. 

## Directions 

1. In a small bowl, whisk together cornstarch and 1/4 cup water; set aside.
1. In a small saucepan over medium heat, add soy sauce, brown sugar, ginger, garlic powder, honey and 2 cup water; bring to a simmer. Stir in cornstarch mixture until thickened enough to coat the back of a spoon, about 2 minutes; let cool to room temperature.
1. In a gallon size Ziploc bag or large bowl, combine half of the teriyaki marinade and chicken; marinate for at least 30 minutes to overnight, turning the bag occasionally.
1. Preheat grill to medium high heat.
1. Thread chicken and vegetables (alternating) onto skewers. Don’t leave any space between the chunks of food. Add skewers to the grill and cook, basting with leftover marinade) until golden brown and cooked through, about 3-4 minutes per side.
1. Serve immediately (with rice if desired), garnished with sesame seeds and green onion. Use marinade that you set aside for sauce. 

[source](http://damndelicious.net/2013/12/20/chicken-teriyaki-kabobs/)