---
title: "Creamy Bowtie Pasta With Asparagus and Mushrooms"
date: 2020-12-22T14:52:58-08:00
categories:
- Entrees
- Pasta
tags:
- mushrooms
- asparagus
- cream
- white wine vinegar
- parmesan cheese
- swiss cheese
- savory leaves
- pasta
---

## Ingredients 

- 1 lb bow tie pasta
- 1 tablespoon olive oil
- 3 cloves garlic, minced
- 3 Tbs. minced onion
- 3 tablespoons butter
- 1 lb trimmed asparagus, cut into 1-inch long pieces
- 6 ounces baby bella or brown button mushrooms, cut into 1/2 inch thick slices
- 1/2 cup whole milk
- 1/4 cup cream
- 1 tablespoon flour
- 1/3 cup white wine, room temperature
- 1/2 cup finely grated parmesan cheese
- 1/4 cup grated swiss cheese
- 1/2 teaspoon salt
- 1/4 teaspoon pepper
- 1/2 teaspoon dried savory leaves, whole or crushed

## Directions 

Prepare the bow tie pasta according to the package directions. Drizzle it with a tablespoon of olive oil and toss, then set aside.

Melt the butter in a large sauté pan over medium heat. Add the garlic and cook for three minutes. Add the asparagus and allow to cook for 5 minutes, stirring every minute or so. Add the mushrooms and cook until they have darkened and begin to wrinkle slightly, about another 5 minutes. Taste one of the asparagus pieces. If it is too hard and stringy inside, cook it a few minutes more. Remove the pan from heat and set it aside.

In a small saucepan, warm the milk and cream over medium-low heat until hot but no boiling. Whisk in the flour until the sauce thickens, making sure the whisk continually until the sauce is smooth and the flour is completely incorporated. Add the wine, grated cheeses, and seasonings and continue stirring until the cheese is melted and the seasonings are evenly distributed throughout the sauce.

Add the pasta and sauce to the sauté pan with the asparagus and mushrooms and toss until the ingredients are evenly distributed and everything is lightly coated in the sauce. Serve immediately.
