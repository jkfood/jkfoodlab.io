---
title: "Yakisoba Chicken"
date: 2020-12-24T14:16:35-08:00
categories:
- Entrees
tags:
- chicken breast
- cabbage
- soba noodles
- carrots
---

## Ingredients 

- 1/2 teaspoon sesame oil
- 1 tablespoon canola oil
- 2 tablespoons chile paste
- 1 tsp. grated ginger
- 2 cloves garlic, chopped
- 2 skinless, boneless chicken breast halves - cut into 1 inch cubes
- 1/2 cup soy sauce
- 1 onion, sliced lengthwise into eighths
- 1/2 medium head cabbage, coarsely chopped
- 2 carrots, coarsely chopped
- 10 ounces soba noodles, cooked and drained


## Directions 

1. In a large skillet combine sesame oil, canola oil and chili paste; stir-fry 30 seconds. Add garlic and stir fry an additional 30 seconds. Add chicken and 1/4 cup of the soy sauce and stir fry until chicken is no longer pink, about 5 minutes. Remove mixture from pan, set aside, and keep warm.
1. In the emptied pan combine the onion, cabbage, and carrots. Stir-fry until cabbage begins to wilt, 2 to 3 minutes. Stir in the remaining soy sauce, cooked noodles, and the chicken mixture to pan and mix to blend. Serve and enjoy!
