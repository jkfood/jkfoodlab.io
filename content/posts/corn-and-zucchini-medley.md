---
title: "Corn and Zucchini Medley"
date: 2020-12-22T14:28:46-08:00
categories:
- Entrees
tags:
- bacon
- veggies
- potatoes
- monterey jack cheese
---

## Ingredients 

- 4 slices bacon
- 2 cups chopped zucchini
- 1-2 sliced potatoes
- 1/2 cups fresh corn kernels
- 1/2 small onion, chopped
- 1 pinch pepper
- 1/4 cup shredded Monterey Jack cheese

## Directions 

1. Place bacon in a large, deep skillet. Cook over medium-high heat until evenly brown. Reserve 1 tablespoon of drippings. Drain bacon, chop, and set aside.
1. Heat the bacon drippings in the skillet over medium heat. Saute the zucchini, corn, and onion until tender but still crisp, about 10 minutes. Season with pepper. Spoon vegetables into a bowl, and sprinkle with chopped bacon and shredded cheese.
