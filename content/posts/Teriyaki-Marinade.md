---
title: "Teriyaki Marinade"
date: 2020-12-22T14:57:20-08:00
categories:
- Marinades
tags: []
---

## Ingredients 

- 1 cup soy sauce
- 1 cup water
- 3/4 cup white sugar
- 1/4 cup Worcestershire sauce
- 3 tablespoons distilled white vinegar 
- 3 tablespoons vegetable oil
- 1/3 cup dried onion flakes
- 2 teaspoons garlic powder
- 1 teaspoon grated fresh ginger


## Directions 

In a medium bowl, mix the soy sauce, water, sugar, Worcestershire sauce, vinegar, oil, onions, garlic powder, and ginger. Stir together until sugar dissolves. Voila - marinade!

*I halved the recipe and it worked great for 3 large chicken breasts. 
