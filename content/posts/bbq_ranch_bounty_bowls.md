---
title: "BBQ Ranch Bounty Bowls"
date: 2020-04-26T21:13:05-07:00
categories:
- Entrees
tags: 
- beans
- chicken breast
- avocado
- yogurt
- rice
- bbq sauce
---

## Ingredients 

- 12.5 oz. raw chicken breast (10 oz. cooked)
- 7 oz. zucchini, sliced into wedges
- 7 oz. bell peppers, seeded and halved
- 2 ears corn on the cob (1 cup corn)
- 14 oz. can of beans
- Sea salt
- Black pepper
- Cumin
- Turmeric
- 1 cup cooked rice
- 120g avocado, chopped
- 4 Tbs. Ranch dressing (yogurt + ranch powder)
- 2 Tbs. BBQ sauce
- 2 Tbs. sunflower seeds, shelled
- 1 lime, juice of

## Directions 

1. Preheat oven to 375 degrees. Line baking sheet with foil. Spray foil with cooking spray. Lay tortillas in a single layer on foil and spray tops with cooking spray. Sprinkle tops of tortillas with taco seasoning. Bake for 8 minutes.
2. Drain and rinse beans. Place on a plate and microwave 30-45 seconds. Mash with a fork, then spread on warm tortillas. Slice bell peppers, grilled chicken and green onions. Layer evenly on tortillas and top each one with 1 Tbs. shredded cheese.
3. Return to oven for another 8 minutes. 2 tostadas per serving. Enjoy each serving warm with 2 oz. guacamole, 1 Tbs. Greek yogurt, 1 Tbs. salsa and a squirt of lime juice.