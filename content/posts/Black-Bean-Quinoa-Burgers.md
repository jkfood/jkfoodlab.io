---
title: "Black Bean Quinoa Burgers"
date: 2020-12-24T14:21:16-08:00
categories:
- Entrees
tags:
- quinoa
- black beans
- poblano
- scallions
- cilantro
- panko
- english muffins
- avocado
---

## Ingredients 

- 1 cup lower-salt chicken or vegetable broth
- 1/2 cup quinoa (white, red, or black), well rinsed
- 1/3 cup plus 2 Tbs. olive oil; more as needed
- 1 small fresh poblano chile, finely chopped
- 4 scallions, thinly sliced
- 2 large cloves garlic, finely chopped
- 1 15-oz. can black beans, drained and rinsed
- 1/2 cup chopped fresh cilantro
- 1/2 cup plain panko
- 1 large egg, beaten
- 1/2 tsp. mild pure chile powder, such as ancho
- 1/4 tsp. ground cumin
- Kosher salt
- 6 English muffins, toasted
- guacamole from 1 avocado

## Directions 
1. In a 2-quart saucepan, bring the broth to a simmer. Add the quinoa, cover, turn the heat down to medium low, and cook until tender and all the liquid is absorbed, 10 to 15 minutes. Remove from the heat and let stand for 3 to 5 minutes. Uncover and fluff with a fork.
2. Heat 2 Tbs. of the oil in an 8-inch skillet over medium heat. Add the poblano, scallions, and garlic, and cook, stirring, until just softened, 2 to 3 minutes. Transfer to a food processor. Add the beans and quinoa, and pulse until the beans are chopped and the mixture is combined.
3. Transfer the mixture to a large bowl and gently stir in the cilantro, panko, egg, chile powder, cumin, and 1/2 tsp. salt. With wet hands, gently form six 1/2-inch-thick patties. Refrigerate, uncovered, until firm, at least 30 minutes and up to 4 hours.

Cook the burgers:
- Heat the remaining 1/3 cup oil in a heavy-duty 12-inch skillet over medium heat until shimmering hot. Cook half of the burgers until well browned on one side, about 2 minutes. Flip and brown the other side, about 1 minute more. Transfer to a plate, cover loosely with foil to keep warm, and repeat with the remaining burgers, adding more oil if necessary. Serve on English muffins with the guacamole.
