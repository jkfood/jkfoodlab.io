---
title: "Honey Lime Chicken Enchiladas"
date: 2020-12-16T19:09:53-08:00
categories:
- Entrees
tags:
- chicken breast
- salsa
---

## Ingredients 

- 4 whole grain tortillas
- 8 oz. cooked chicken breast, shredded
- 1⁄4 cup fresh minced onion
- 1⁄4 cup nonfat, plain Greek yogurt
- 2 Tbs. honey
- 2 Tbs. lime juice
- 1 tsp. chili powder
- Dash garlic powder
- 1⁄2 cup green enchilada sauce
- 2 lite Laughing Cow cheese wedge
- 1⁄4 cup low-fat, shredded mozzarella cheese
- 4 Tbs. fresh Pico De Gallo or salsa
- 32 asparagus spears
- 2 tsp. olive oil
- Dash of sea salt + pepper

## Directions 

1. Add Greek yogurt, honey, lime juice, chili powder and garlic powder to a large Ziploc bag. Massage ingredients together until well combined. Add cooked, shredded chicken and onion to the bag. Massage all ingredients until well combined. Store in the fridge and let marinate 2+ hours.
1. Preheat oven to 350 degrees. Weigh chicken mixture and divide by 4. Fill each tortilla with 1⁄4 of the chicken mixture. Roll up tight and place in a greased 9x13 baking dish.
1. Beat the Laughing Cow cheese until smooth, then beat in the enchilada sauce. Pour the mixture over the top of the tortillas and then sprinkle with shredded mozzarella.
1. Bake for 30 minutes. Top with fresh salsa, salt and pepper to taste.
1. Turn oven heat up to 400. Place asparagus on a baking sheet lined with parchment paper. Drizzle olive oil over the top and sprinkle with salt and pepper. Roast for 8 minutes. Enjoy on the side.