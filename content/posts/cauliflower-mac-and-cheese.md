---
title: "Cauliflower Mac and Cheese"
date: 2020-12-16T19:43:04-08:00
categories:
- Entrees
tags:
- cauliflower
- parmesan cheese
---

## Ingredients 

- 1 head of cauliflower (cut into noodle-sized florets) roasted in a 400 degree oven for 15 minutes
- 8 oz whole wheat macaroni/elbow pasta, cooked
- 12 oz  Italian/andouille chicken sausage, cut in half lengthwise then thinly sliced and browned

Sauce:
- 2 Tbs. butter
- 3 Tbs. all purpose flour
- 2 cups milk
- ½ tsp salt
- Black pepper
- 1 cup shredded sharp cheddar cheese (not extra sharp)
- ½ cup shredded parmesan cheese
- 2 Tbs nutritional yeast (optional)

- Topping:
- 4 Tbs parmesan cheese
- ⅓ cup bread crumbs 
- Dash of salt, paprika, garlic powder, and thyme

## Directions 

1. Boil and drain noodles, roast cauliflower, and brown the sausage.
1. Heat oven to 350. Grease a 9 x 13 casserole dish.
1. Make the sauce:
1. Melt butter in a large saucepan at low/medium heat. 
1. Add the flour and stir until well combined. 
1. Slowly whisk in the milk and stir constantly until slightly thickened. 
1. Add the salt, pepper, and cheese. Stir until homogenous 
1. Add cauliflower, noodles, and sausage to the pot. Stir together. Pour mixture into the 9x13.
1. Sprinkle combined topping ingredients over the top and spray with oil spray. 
1. Bake for 20 minutes at 350.
