---
title: "Cottage Pie With Beef and Carrots"
date: 2020-12-22T21:05:49-08:00
categories:
- Entrees
tags:
- chuck
- mushrooms
- potatoes
---

## Ingredients 

- 1-3/4 cups low-salt beef broth (1 14-oz. can) 
- 1/2 oz.dried porcini mushrooms (optional)
- 2 to 3 Tbs. olive or vegetable oil 
- 2-1/2 lb. thin-cut chuck steaks, preferably top blade (or flat iron), 1/2 to 3/4 inch thick, trimmed of any excess fat or gristle 
- Kosher salt and freshly ground black pepper 
- 3 medium carrots, peeled and cut into 1/2-inch dice (about 1-1/3 cups) 
- 2 celery stalks, cut into 1/2-inch dice (about 1 cup) (optional)
- 2 small onions, cut into 1/2-inch dice (about 2 cups) 
- 11/2 teaspoons fresh thyme leaves (or 1/2 tsp. dried) 
- 2 Tbs. tomato paste 
- ⅓ cup balsamic vinegar
- For the topping 
- 2 lb. russet potatoes (3 to 4 medium), scrubbed and cut into 1-1/2- to 2-inch chunks 
- Kosher salt 
- 5 Tbs. unsalted butter (at room temperature, cut into 3 pieces) plus 2 tsp. (cold, cut into small bits); more for the baking dish 
- 1/2 cup milk, light cream, or half-and-half, warmed 
- Freshly ground black pepper 

## Directions 

Stew:
1. braise the meat
2. Put all the stew ingredients in the crockpot on low for 5 hours (You can leave carrots out until the last hour if you don’t want them to be mushy.)

Make the topping:
1. About 30 minutes before the stew is ready, put the potatoes in a large saucepan and cover by an inch with cold water. Add 1 tsp. kosher salt. Bring to a simmer over medium heat, partially cover, and simmer until the potatoes are easily pierced with a skewer, about 20 minutes. Drain, and return the potatoes to the saucepan. Put the pan over low heat and shake or stir the potatoes until a floury film forms on the bottom of the pot, 1 to 2 minutes.
1. Using a ricer, food mill, or potato masher, mash the potatoes. Stir in the 5 Tbs. of butter with a broad wooden spoon. Once the butter is thoroughly absorbed, add the milk or cream in three parts, stirring vigorously between additions. Season to taste with salt and pepper.
1. Assemble and bake:
1. Lightly butter a 9x13 casserole dish. Shred the stew meat if necessary. Spoon the stew into the baking dish. Spread the potatoes on top in an even layer—you don’t need to spread them all the way to the edge. 
1. Bake at 375°F until the stew is bubbling around the sides, and the top is lightly browned, 35 to 45 minutes (45 to 55 minutes if the pie has been refrigerated).
