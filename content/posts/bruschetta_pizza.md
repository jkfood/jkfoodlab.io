---
title: "Bruschetta Pizza"
date: 2020-04-26T21:13:05-07:00
categories:
- Entrees
tags: 
- spaghetti squash
- chicken breast
---

## Ingredients 

- 4 pieces Stonefire mini naan bread (or pizza crust)
- 1⁄2 cup marinara sauce
- 1⁄2 cup shredded mozzarella cheese
- 10 oz. raw chicken breast (8 oz. grilled)
- Dash paprika
- Dash garlic powder
- Dash onion powder
- 6 oz. tomatoes, chopped
- 4 oz. cucumbers, chopped
- 1 Tbs. olive oil
- 1⁄2 tsp. minced garlic
- Dash sea salt
- Dash ground black pepper
- Pinch fresh basil, chopped
- 2 Tbs. grated parmesan cheese
- 2 Tbs. balsamic glaze

## Directions 

1. Make pizza dough and heat oven (+ pizza stone) to 500 degrees. 
2. Salt, pepper, and grill chicken breast halves.
3. Spread 2 Tbs. Marinara sauce over rolled out crust then add mozzarella and grilled chicken to each pizza. Sprinkle with paprika, garlic powder and onion powder. Bake until cheese is melted.
4. Combine the tomatoes, cucumbers, olive oil, minced garlic, sea salt, pepper and fresh basil together in a bowl. Evenly top pizzas with the bruschetta salad mixture, Parmesan cheese and balsamic glaze.