---
title: "Ham and Broccoli Quiche"
date: 2020-12-22T21:11:08-08:00
categories:
- Entrees
tags:
- ham
- broccoli
- eggs
---

## Ingredients 

Filling:
- 5 eggs
- ½ cup milk 
- 1 tsp thyme
- ½ cup plain yogurt
- 1/4 ground nutmeg
- ½ tsp salt
- ¼ tsp pepper
- 3/4 cup grated Swiss cheese (maybe even ½ cup?)
- 2 Tbs grated cheddar
- 1/2 cup bite-size broccoli florets, steamed 
- 1/2 cup diced ham or bacon!!

## Directions 

Blind bake a pie crust or oil and flour a pie pan. 

Whisk flour with salt in a bowl; use a fork to stir in vegetable oil. Mix in water, 1 tablespoon at a time, before adding more water. Gather dough into a ball, divide in half, and roll out on a floured work surface. Use a fork to poke holes in the bottom of the crust so it won’t balloon up and cook it for 15ish minutes at 350.

You’ll know it’s done when the egg mixture doesn’t jiggle anymore in the middle and when the top is a nice golden brown. It will also puff up. Let it set to continue cooking. 

[source](http://www.finecooking.com/recipes/south-indian-style-vegetable-curry.aspx?nterms=72340)
