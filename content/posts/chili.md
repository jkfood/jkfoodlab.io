---
title: "Chili"
date: 2020-12-24T14:52:09-08:00
categories:
- Entrees
tags:
- Rump Roast
- green chilis
---

## Ingredients 

- 1.5 lb rump roast, cubed
- 1 can petite diced tomatoes
- 1 can beef broth
- 1 can green chiles?
- 1 tsp chili powder (more?)
- 1 tsp cumin (more?)
- 3 cloves garlic, minced
- 1 small-med onion, diced
- 1 can pinto beans, drained
- 1 can black beans, drained
- ½ cup corn?

## Directions 

1. Cube rump roast (discarding big pieces of fat). Cube it and toss with salt and pepper. Sear in a hot cast iron greased with bacon grease. Work in shifts to not overcrowd the pan. Add meat to crock pot. 
1. In the same pan, saute onions and add garlic. Add to crock pot. 
1. Pour half of beef broth in the pan and scrape up bits of meat. Add that to crock pot. 
1. Add the rest of the beef broth, tomatoes, chiles, and spices to the crockpot. 
1. Cook on low for 10 hours? 
1. Add beans and corn. 
