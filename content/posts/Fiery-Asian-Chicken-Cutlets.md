---
title: "Fiery Asian Chicken Cutlets"
date: 2020-12-22T14:58:58-08:00
categories:
- Marinades
- Chicken
tags:
- chicken breast
---

These spicy chicken cutlets can be served with rice or salad, sliced into pasta salads, and they make terrific sandwiches on crusty bread with a bit of mayo. They can be marinated in advance and cook in less than 10 minutes under the broiler. They can also be grilled or pan-fried on a skillet. 

## Ingredients 

- 1/4 cup fish sauce (you can also substitute a teaspoon of anchovy paste stirred into another 1/4 cup of soy sauce) 
- 1/4 cup rice wine vinegar (you can also use red wine vinegar)
- 1/4 cup low sodium soy sauce
- 1/4 cup sriracha sauce
- 1/4 cup olive oil
- 1 lime 
- 1/2 teaspoon ground cayenne
- 4 garlic cloves, peeled and smashed
- 1- 1 1/2 pounds boneless, skinless chicken cutlets (about 4 to 6 pieces), rinsed then patted dry with paper towel and cut lengthwise in half
- Kosher salt
- Black pepper

## Directions 

Whisk together the first five ingredients in a medium-sized storage container with a lid (such as Tupperware). Cut the lime in half and squeeze in the juice, then drop in the squeezed lime halves. Stir in the ground cayenne, smashed garlic, and kosher salt. Add the chicken cutlets turning each until full coated. Cover the storage container and let marinate in the refrigerator at least 1 hour (or overnight).

