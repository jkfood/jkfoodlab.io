---
title: "Lemon Garlic Chicken Pasta"
date: 2020-12-24T14:36:06-08:00
categories:
- Entrees
- Pasta
tags:
- chicken breast
- parmesan cheese
- tomatoes
---

## Ingredients 

- 8 oz pasta
- 3 tbs olive oil, divided
- 1 clove garlic (grated)
- 1 can petite diced tomatoes, drained
- 1-2 tsp. dried parsley (fresh and not chopped)
- 2 Chicken breasts cut into 1" cubes, or smaller
- salt/pepper to taste
- red pepper flakes
- 2 Tbs. Lemon juice
- grated parmesan cheese 

## Directions 

Set a pot of water to boil for the pasta. 

In a small pot/pan, heat 2 Tbs. olive oil. Add garlic. After it is fragrant, add the tomatoes and parsley and simmer on med-low. 

In a frying pan, heat remaining 1 Tbs. oil over high heat. Once the oil is shimmering, add chicken. Make sure to separate the pieces in the pan. This helps give it a golden color. Season with salt, pepper, and red pepper flakes. 

Once tomatoes are soft, add lemon juice and mix thoroughly. Combine with chicken and pasta. Serve with parmesan cheese on top. 
