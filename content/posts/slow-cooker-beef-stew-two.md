---
title: "Slow Cooker Beef Stew Two"
date: 2020-12-24T14:53:44-08:00
categories:
- Entrees
tags:
- chuck roast
- red wine vinegar
- potatoes
- carrots
- celery
---

## Ingredients 

- 2 pounds beef stew meat, cut into 1 inch cubes, salted and peppered
- 1/2 teaspoon seasoned salt
- 1/2 teaspoon ground black pepper
- 3 Tbs. red wine vinegar
- 2 cloves garlic, minced
- 2 bay leaves
- 1 teaspoon paprika
- 1 tablespoon Worcestershire sauce
- 2 cans beef broth
- 1 onion, chopped
- 1-3 potatoes, diced
- 4 carrots, sliced
- 2  stalks celery, chopped
- Frozen peas?

## Directions 

1. Sear the meat in shifts, careful to not crowd. Put in the crock pot with the seasonings. Reserve the veggies for an hour or two before adding. 
1. Cover, and cook on Low setting for 10 to 12 hours, or on High setting for 4 to 6 hours.
