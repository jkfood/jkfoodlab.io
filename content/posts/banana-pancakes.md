---
title: "Banana Pancakes"
date: 2020-12-16T20:17:10-08:00
categories:
- Breakfast
- Pancakes
tags:
- bananas
---

## Ingredients 

dry:
- 1 cup all-purpose flour
- 2 teaspoons baking powder
- 1 teaspoon baking soda
- 1/4 teaspoon salt
- 3/4 cup milk (if you have sour cream, yogurt, or buttermilk, use it instead!)
- 3 tablespoons butter, melted
- 2 eggs

wet:
- 1 tablespoon brown sugar
- 1 teaspoon vanilla extract
- 1 large banana, smashed
- frozen/fresh blueberries

## Directions 

1. Combine flour, baking powder, baking soda, and salt in a large bowl. Set bowl aside. In a separate bowl, whisk together the milk, melted butter, eggs, sugar, bananas, and vanilla. Make a well in the center of the dry ingredients and stir in the wet ingredients, being careful not to overmix the batter.  Let batter sit 5-10 minutes if I you want them fluffy! 
2. Heat a large skillet over medium heat, and coat with cooking spray. Pour 1/4 cupfuls of batter onto the skillet. If using blueberries, scatter a handful of them on the raw pancake. Cook until bubbles appear on the surface. Flip with a spatula, and cook until browned on the other side.
