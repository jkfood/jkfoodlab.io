---
title: "Curry Chicken Salad"
date: 2020-12-24T14:25:07-08:00
categories:
- Entrees
tags:
- chicken breast
- green onions
- pecans
- celery
- grapes
---

## Ingredients 

- 4 skinless, boneless chicken breasthalves - cooked and diced
- 1 stalk celery, diced
- 4 green onions, chopped
- 1/3 cup seedless green grapes, halved
- 1/2 cup chopped toasted pecans
- 1/8 teaspoon ground black pepper
- 1/2 teaspoon curry powder
- 3/4 cup light mayonnaise

## Directions 
1. In a large bowl combine the chicken, celery, onion, apple, raisins, grapes, pecans, pepper, curry powder and mayonnaise. Mix all together. Serve!
