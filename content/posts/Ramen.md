---
title: "Ramen"
date: 2020-12-16T19:40:09-08:00
categories:
- Entrees
- Soup
tags:
- chicken breast
- egg noodles
- carrots
- mushrooms
- seaweed
---

## Ingredients 

- 3-4 eggs
- 1 tsp sesame oil
- 1 cup carrots, diced
- 4+ cloves garlic, mince
- 1” ginger, minced
- 1 cup Mushrooms, minced
- 4 cups chicken broth (low sodium)
- 1-2 cups diced raw chicken (can be breast, wing, etc)

- Water
- 2 Ramen Noodles packet 

- ¼ cup soy sauce
- 2 Tbs mirin (sweet rice wine for cooking)
- Seaweed cut into .5” x 1.5” strips (or I have added spinach cut small to the pot)

## Directions 
1. Soft boil eggs. Set to cool in cold water. (Peel when cool and it’s convenient.)
2. Saute carrots, garlic, ginger, mushrooms in a pot until fragrant. Boil a separate pot of water. 
3. Add broth and chicken to veggies and boil until chicken is cooked. Let steep on low.
4. Add just the noodles from a Ramen Noodles packet to the boiling water. Discard seasoning packet.  Once noodles are almost done, drain them in a colander. 
5. Combine soy sauce and mirin. Add to soup to taste. Try adding noodles just to portions and storing separately so they don’t get soggy. Garnish with green onions and seaweed.