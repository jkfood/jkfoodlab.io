---
title: "Roasted Roots and Smoked Sausage"
date: 2020-12-24T14:34:36-08:00
categories:
- Entrees
tags:
- potatoes
- carrots
- parsnips
- turnips
- beets
- fresh thyme
- fresh rosemary
---

## Ingredients 

- ½ lb. potatoes (sweet!)
- 5 carrots/parsnips (a combination) scrubbed and cubed
- 1 red onion peeled and cut into wedges
- 1-2 turnips peeled and cut into cubes or wedges
- 2 beets peeled and cut into cubes or wedges
- 5+ cloves of garlic (up to a whole head) peeled, and halved
- 2 Tablespoons fresh thyme, minced
- 2 teaspoons fresh rosemary, minced
- 3 Tablespoons extra virgin olive oil
- salt and pepper to taste

## Directions 

1. Preheat oven to 375°F.
2. Place potatoes, carrots, parsnips, turnips, and red onions in a large bowl and toss together.
3. With the side of a knife, gently smash each clove of garlic and add it to the bowl of vegetables.
4. Add the herbs and olive oil to the vegetables and generously season with salt and pepper.
5. Pour the seasoned vegetables onto a large roasting pan and spread in a single layer.
6. Roast vegetables in the oven for 30 to 40 minutes or until golden brown. (Gently stirring the vegetables halfway through the cooking time) Serve.
