---
title: "Soy Sauce Chicken Marinade"
date: 2020-12-22T14:55:12-08:00
categories:
- Marinades
- Chicken
tags:
- fresh parsley
- red wine vinegar
- lemon juice
---

## Ingredients 

- 1 1/2 cups vegetable oil
- 3/4 cup soy sauce
- 1/2 cup Worcestershire sauce
- 1/2 cup red wine vinegar
- 1/3 cup lemon juice
- 2 tsp. dijon mustard
- 1 teaspoon salt
- 1 tablespoon black pepper
- 1 1/2 teaspoons finely minced fresh parsley

## Directions 

In a medium bowl, mix together oil, soy sauce, Worcestershire sauce, wine vinegar, and lemon juice. Stir in mustard powder, salt, pepper, and parsley. Use to marinate chicken before cooking as desired. The longer you marinate, the more flavor it will have.