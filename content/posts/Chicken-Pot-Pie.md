---
title: "Chicken Pot Pie"
date: 2020-12-22T21:17:30-08:00
categories:
- Entrees
tags:
- chicken breast
- carrots
- broccoli
- celery
---

## Ingredients 

- 1 pound skinless, boneless chicken breast halves - cubed
- 1 cup sliced carrots
- 1 cup cut up broccoli
- 1/2 cup sliced celery
- 2/3 cup butter
- 2/3 cup chopped onion
- 2 cloves garlic
- 2/3 cup all-purpose flour

- ¾  teaspoon salt
- ¾  teaspoon black pepper
- 3 1/2 cups chicken broth
- 1 1/3 cup milk

- 2 (9 inch) unbaked pie crusts
- red pepper flakes
- thyme
- parsley

## Directions 

1. Preheat oven to 425 degrees F (220 degrees C.)
1. In a saucepan, combine chicken, carrots, broccoli, and celery. Add broth to cover and boil for 15 minutes. Remove from heat, drain and set aside.
1. In the saucepan over medium heat, cook onions in butter until soft and translucent. Stir in flour, salt, pepper, and celery seed. Slowly stir in drained chicken broth (and maybe more) and milk. Simmer over medium-low heat until thick. Remove from heat and set aside.
1. Place the chicken mixture in bottom pie crust. Pour hot liquid mixture over. Cover with top crust, seal edges, and cut away excess dough. Make several small slits in the top to allow steam to escape.
1. Bake in the preheated oven for 30 to 35 minutes, or until pastry is golden brown and filling is bubbly. Cool for 10 minutes before serving.

yield: ⅕ pies. 
