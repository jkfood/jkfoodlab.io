---
title: "Apple Buttermilk Pancakes"
date: 2020-12-16T20:15:42-08:00
categories:
- Breakfast
- Pancakes
tags:
- Apples
---

## Ingredients 

- 1 c. sifted whole wheat flour (or white)
- ½ tsp. baking soda
- ¼ tsp. salt 
- 2 tbsp. brown sugar
- 1 well beaten eggs
- 1 c. buttermilk
- 1 tbsp. canola oil or melted butter
- 1 c. pared and finely chopped apples
- 2 tsp cinnamon

## Directions 

Sift flour, baking soda, salt, and sugar. Combine eggs, buttermilk, and oil/butter. Add gradually to dry ingredients. Stir until smooth. Fold in apples (8 cakes)