---
title: "Asian Chicken or Turkey Wraps"
date: 2020-12-22T14:33:11-08:00
categories:
- Entrees
tags:
- chicken breast
- jalapeno
- lettuce
- fresh basil
---

## Ingredients 

- 1 1/2 cups water
- 1 cup short grain rice (i.e. sushi rice), rinsed (rice is optional--helpful to soak up flavors and make meal more filling)
- 1/2 teaspoon salt
- 2 tablespoons fish sauce
- 1 tablespoon reduced-sodium soy sauce
- 3 tablespoons fresh lime juice 
- 1 1/2 tablespoons brown sugar
- 1 teaspoon cornstarch
- 1 pound minced chicken breast
- 2 teaspoons vegetable or canola oil
- 1 jalapeno stemmed, seeded, and minced (leave in the seeds if you like more heat) (1.5 tsp. Of canned)
- 2 teaspoons fresh lime zest (from about 2 limes)
- 1/4 cup chopped fresh basil (or 1-2 tsp dried basil)
- 3 green onions, finely chopped
- 12 Bibb or Boston lettuce leaves (about 1 head), washed (I used the small romaine leaves in the center of the head)

## Directions 

1. In a medium saucepan, bring the water, 1/2 teaspoon salt, and rice to a boil high heat. Cover, reduce the heat to low, and cook for 10 minutes. Remove the rice from the heat and let sit, covered, until tender, another 10-15 minutes.
1. Whisk the fish sauce, soy sauce, lime juice, brown sugar, and cornstarch together in a small bowl and set aside.
1. Heat the oil in a 12-inch nonstick skillet over medium heat until shimmering. Add the chicken, jalapeno, and lime zest and cook, breaking up the meat into small pieces with a wooden spoon, until the chicken is no longer pink, about 5 minutes.
1. Whisk the fish sauce/lime mixture to recombine; add it to the skillet and cook over medium-high heat, stirring constantly, until the sauce has thickened, a minute or two. Off the heat, stir in the fresh basil and scallions. To serve, open up a lettuce leaf, place a small scoop of rice on the bed of lettuce and top with some of the warm chicken mixture.

[source](http://www.melskitchencafe.com/asian-chicken-lettuce-wraps-quick-and-delish/)