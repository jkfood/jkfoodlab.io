---
title: "Grilled Flank Steak Salad"
date: 2020-12-22T14:40:19-08:00
categories:
- Entrees
- Salad
tags:
- balsamic vinigar
- fresh thyme
- fresh marjoram
- flank steak
- lettuce
- tomatoes
---

## Ingredients 

For the mustard-herb vinaigrette:
- 1/3 cup balsamic vinegar
- 1 1/2 Tbs. chopped fresh thyme
- 1 1/2 Tbs. chopped fresh marjoram
- 1 1/2 Tbs. Dijon mustard
- 2 large garlic cloves, minced
- 3/4 tsp. salt
- 3/4 tsp. freshly ground pepper
- 3/4 cup extra-virgin olive oil

* 1 flank steak, about 1 1/2 lb.
* 1 red onion, cut into wedges
* 1 large head romaine lettuce, leaves torn into bite-size pieces
* 2 or 3 tomatoes, preferably heirloom, cut into wedges, plus a handful of mixed cherry tomatoes, halved


## Directions 

1. To make the vinaigrette, in a small bowl, whisk together the vinegar, thyme, marjoram, mustard, garlic, salt and pepper. Add the olive oil in a thin stream, whisking constantly until the vinaigrette is well blended.
1. Place the steak in a shallow dish. Pour half of the vinaigrette over the steak and turn to coat both sides. Cover and refrigerate for at least 4 hours or up to 24 hours, turning the steak over occasionally. Cover and refrigerate the remaining vinaigrette. Remove the steak from the refrigerator 30 minutes before grilling.
1. Prepare a hot fire in a grill.
1. Remove the steak from the marinade, reserving the marinade. Grill the steak, turning it over once or twice and brushing with the reserved marinade for up to 5 minutes before the steak is done, until nicely charred and cooked to your liking, 10 to 12 minutes total for medium-rare. Transfer the steak to a cutting board, cover loosely with aluminum foil and let rest for 5 to 10 minutes.

1. Meanwhile, grill the onion wedges until softened and nicely grill-marked, about 5 minutes. Thinly slice the steak across the grain, reserving any accumulated juices. Toss the lettuce with the reserved vinaigrette and divide among individual plates. Top with the steak, onion wedges and tomatoes. Drizzle the steak with the meat juices and serve immediately. 
