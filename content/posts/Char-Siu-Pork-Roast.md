---
title: "Char Siu Pork Roast"
date: 2020-12-23T18:07:41-08:00
categories:
- Entrees
tags:
- hoisin sauce
- pork butt
---

In this recipe for Char Siu, the Chinese version of barbecue, the pork is cooked in the slow-cooker for an extra tender, juicy, and stress-free meal. Serve with sticky or long-grain white rice and a steamed or stir-fried medley of bell peppers, carrots, snow peas, sliced baby corn, and water chestnuts.
Cooking Light MARCH 2006
Yield: 8 servings (serving size: 3 ounces pork and 1/4 cup sauce)

## Ingredients 

- 1/4 cup lower-sodium soy sauce
- 1/4 cup hoisin sauce
- 3 tablespoons ketchup
- 3 tablespoons honey
- 2 teaspoons minced garlic
- 2 teaspoons grated peeled fresh ginger
- 1 teaspoon dark sesame oil
- 1/2 teaspoon five-spice powder
- 1 (2-pound) boneless pork shoulder (Boston butt), trimmed
- 1/2 cup fat-free, lower-sodium chicken broth

## Directions 

Combine first 8 ingredients in a small bowl, stirring well with a whisk. Place in a large zip-top plastic bag. Add pork to bag; seal. Marinate in refrigerator at least 2 hours, turning occasionally.
Place pork and marinade in an electric slow cooker. Cover and cook on low for 8 hours.
Remove pork from slow cooker using a slotted spoon; place on a cutting board or work surface. Cover with aluminum foil; keep warm.
Add broth to sauce in slow cooker. Cover and cook on low for 30 minutes or until sauce thickens. Shred pork with 2 forks; serve with sauce.
