---
title: "Chickpea Carrot and Parsley Salad"
date: 2020-12-22T14:30:43-08:00
categories:
- Entrees
- Salad
tags:
- chickpeas
- fresh parsley
- radishes
- scallions
- pine nuts
---

Serves four to six as a vegetarian main dish; eight as a side dish.

## Ingredients 

- 19-oz. can chickpeas, drained and rinsed (about 2 cups)
- 1 cup loosely packed fresh flat-leaf parsley leaves, very coarsely chopped
- 1 cup loosely packed shredded carrot (from about 1 large carrot)
- 1/2 cup sliced radishes (about 6 medium)
- 1/2 cup chopped scallions, white and green parts (about 4)
- 3 Tbs. fresh lemon juice
- 1 tsp. ground coriander
- Kosher salt and freshly ground black pepper
- 6 Tbs. extra-virgin olive oil
- 1/3 cup crumbled feta cheese or toasted pine nuts (optional)


## Directions 

1. Put 1/2 cup of the chickpeas in a mixing bowl and mash them into a coarse paste with a potato masher or large wooden spoon. Toss in the remaining chickpeas along with the parsley, carrot, radishes, and scallions. Stir to combine.
1. In a liquid measuring cup, whisk together the lemon juice, coriander, 1/2 tsp. salt, and a few generous grinds of black pepper. Continue whisking while adding the olive oil in a slow stream. Pour over the salad and toss gently. Season the salad with salt and pepper to taste. Top with the feta or pine nuts, if using, and serve immediately.Serving Suggestions:Serve with warmed pita bread, sliced into wedges.
