---
title: "Shredded Beed Chimichangas"
date: 2020-12-24T14:37:25-08:00
categories:
- Entrees
tags:
- chuck roast
- red wine vinegar
- hot sauce
- sour cream
- salsa
- monterey jack cheese
- flour tortillas
---

## Ingredients 

- 2 pounds boneless beef chuck roast, trimmed of fat
- 1 cups beef broth 
- 3 tablespoons red wine vinegar (maybe try a ¼ cup?)
- 2 tablespoons chili powder
-  ½ Tbs. ground cumin
- ⅛ tsp. Salt
- ⅛ tsp. Garlic powder
- ½ tsp. Tapatio or other hot sauce
- 1 can beans (I used pinto)
- 8-10 flour tortillas
- 3 tablespoons butter, melted
- ½-1 cup salsa
- 1/2 cup shredded Monterey Jack cheese
- 1/2 cup sour cream

## Directions 

1. Coat a cast iron skillet with bacon grease or vegetable oil and heat over medium-high on a stove. 
1. Trim the roast of fat and pat dry. Cook for 1-2 minutes (sear) in the skillet, on both sides then place the roast in the crock pot. 
1. In a medium bowl, combine beef broth, red wine vinegar, chili powder, cumin, garlic, and salt. Pour over beef. 
1. Cover, and cook until meat is very tender, and pulls apart easily, about 5 hours on high or 8 hours on low. 
1. Use a slotted spoon to transfer the meat to a mixing bowl. Add salsa and more salt/garlic/hot sauce to taste. Add beans.
1. Preheat oven to 500 degrees F (260 degrees C).
1. Brush both sides of each tortilla with melted butter. Spoon cheese and shredded beef filling down center of each tortilla. Fold ends over filling, then fold sides to center to make a packet. Place chimichangas, seam side down, in a 9- by 13-inch baking pan. (I used a cookie sheet)
1. Bake in preheated oven for 8 to 10 minutes, or until golden brown. Serve with sour cream (or chipotle sour cream or fiesta ranch), diced tomatoes, and lettuce. 
