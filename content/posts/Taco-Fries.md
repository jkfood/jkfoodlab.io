---
title: "Taco Fries"
date: 2020-12-16T19:22:18-08:00
categories:
- Entrees
tags:
- red potatoes
- ground turkey
- red bell pepper
- guacamole
- jalapeno
---

## Ingredients 

- 10 oz. baby red potatoes
- Garlic powder
- Onion powder
- 14 oz. lean ground turkey
- 2 Tbs. taco seasoning
- 1⁄4 of an onion, minced
- 1⁄4 of a red pepper, minced
- Toppings per serving:
- 1 Tbs. shredded, low-fat mozzarella cheese
- 1⁄2 Tbs. guacamole
- 1⁄2 Tbs. Cashew Sour Cream (recipe in Food Prep Guide)
- Jalapeno pepper, chopped (optional)
- Green onions, for garnish

## Directions 

1. Preheat oven to 425 degrees. Slice potatoes into fries and lay out onto a baking sheet lined with parchment paper in a single layer. Spray tops with cooking spray and sprinkle with sea salt, onion powder and garlic powder. Bake for 20 minutes. Flip and bake another 15-20 minutes.
1. Brown turkey in a frying pan or skillet over medium heat. Once cooked, add taco seasoning. Remove from heat. Add onions and peppers to the pan and cook until tender. Add to the turkey mixture.
1. Weigh the potatoes and turkey mixture separately and divide by 4 to get the amount needed for one serving. Place fries on a plate topped with turkey mixture, cheese, guacamole, sour cream, jalapenos and green onions. Enjoy!