---
title: "Slow Cooker Five Spice Pork With Snap Peas"
date: 2020-12-24T14:13:56-08:00
categories:
- Entrees
tags:
- rice wine
- five-spice
- pork shoulder
- sugar snap peas
---

Chinese five-spice powder, garlic, ginger, and Asian chili sauce supercharge this sweet and spicy pork stew with flavor. The prep work for the dish is super quick, and the rest of the time is hands-off. Simply start it earlier in the day to enjoy a dinner that requires no work later. Serve with white or brown rice.

serves 4

## Ingredients 

- 1/2 cup Shaoxing (Chinese rice wine) or dry sherry
- 1/2 cup reduced-sodium soy sauce
- 1/2 cup light brown sugar
- 1 Tbs. Asian chili sauce, such as Sriracha
- 2 large cloves garlic, minced
- 1 tsp. grated fresh ginger
- 1 tsp. Chinese five-spice powder
- 2 lb. boneless pork shoulder, trimmed of excess fat, and cut into 1-inch pieces
- Freshly ground black pepper
- 1/2 lb. sugar snap peas (fresh or frozen), trimmed

## Directions 

*by Julissa Roberts from Fine Cooking Issue 121*

1. In a 5- to 6-quart slow cooker, combine the Shaoxing, soy sauce, brown sugar, chili sauce, garlic, ginger, five-spice powder, mh and 1/2 cup water.
1. Lightly season the kqpork qwith pepper. Add the pork to the slow  spring and hm
1. We break s mcnagqkkpcooker and stir to coat. Cook, covered,on until fork-tender, 4 to 5 hours on high or 6 to 7 hours on low.
1. Add the sugar snap peas and cook until crisp-tender, about 10 minutes. Serve.


nutrition information (per serving): 
Calories (kcal): 480; Fat (g): 17; Fat Calories (kcal): 160; Saturated Fat (g): 7; Protein (g): 47;
Monounsaturated Fat (g): 8; Carbohydrates (g): 24; Polyunsaturated Fat (g): 2; Sodium (mg): 1250;
Cholesterol (mg): 140; Fiber (g): 2;
