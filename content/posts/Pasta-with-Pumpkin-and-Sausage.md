---
title: "Pasta With Pumpkin and Sausage"
date: 2020-12-24T14:27:10-08:00
categories:
- Entrees
- Pasta
tags:
- italian sausage
- onion
- pumpkin
- marjoram
- kale
- parmesan cheese
- pasta
---
**Serves 6**
*by Jeanne Kelley from Fine Cooking Issue 107*

This hearty fall dish is a wonderful way to incorporate fresh pumpkin into a simple but thoroughly satisfying midweek meal.

## Ingredients 

- Kosher salt
- 1 lb. sweet Italian sausage, casings removed if using links
- 1 medium yellow onion, chopped
- 4 cups 3/4-inch-diced peeled, seeded pumpkin
- 4 cloves garlic, minced
- 1 tsp. dried marjoram
- 1-3/4 cups lower-salt chicken broth
- 5 leaves of kale, trimmed, ribs removed, leaves cut into 1-inch pieces
- 6 oz. dried campanelle (I used bowtie) pasta
- 1/2 cup grated Parmigiano-Reggiano (I used parmesan); more for serving
- Freshly ground black pepper

## Directions 

Bring a large pot of well-salted water to a boil over high heat.
In a heavy-duty 12-inch skillet over medium heat, cook the sausage, breaking it up into small pieces with a wooden spoon, until mostly browned, 6 to 8 minutes. Push the sausage towards the edge of the skillet and add the olive oil if the center of the pan is dry (this will depend on the amount of fat in the sausage). Add the onion and cook until golden and the sausage is well browned, about 8 minutes. Stir in the pumpkin, ¼ c of the broth, garlic, and marjoram and cook until the wine evaporates, 3 to 4 minutes. Add 3/4 cup of the broth and cook until the pumpkin is almost tender, about 8 minutes. Add the kale as nd the remaining 3/4 cup broth, cover, and cook until the pumpkin and kale are tender, about 4 minutes.
Meanwhile, cook the pasta in the boiling water according to package directions until al dente. Reserve 1/2 cup of the pasta water and then drain well. Add the pasta to the skillet with enough of the pasta water to coat the pasta and vegetables generously. Stir in the Parmigiano and season to taste with salt and pepper. Serve with additional Parmigiano.
Serving Suggestions

Serve with an [Arugula & Fennel Salad](http://www.finecooking.com/recipes/arugula-fennel-salad-orange-hazelnuts.aspx).