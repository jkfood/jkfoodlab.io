---
title: "Quinoa Black OliveCakes With Baby Greens and Roasted Red Pepper Sauce"
date: 2020-12-24T14:18:47-08:00
categories:
- Entrees
tags:
- quinoa
- fresh thyme
- olives
- red bell peppers
- baby greens
---

Love quinoa but want to expand your repertoire beyond the usual grain salad or pilaf? These crisp-crusted quinoa cakes provide a mild canvas for the bold flavors of olives, peppers, and greens. With white beans to give them some heft and heartiness, they make a terrific meatless main course.

## Ingredients 

- 1 cup white quinoa
- 2 cups lower-salt chicken or vegetable broth
- 1 cup cooked white beans (from a 15 oz. can), drained and rinsed
- 2 large eggs
- 3 peeled medium cloves garlic, 2 minced, 1 left whole
- 2 tsp. finely chopped fresh thyme
- Kosher salt and freshly ground black pepper
- 1 cup coarse fresh white breadcrumbs (see below)
- 1/2 cup chopped flat-leaf parsley
- 1/3 cup pitted kalamata olives, finely cihopped
- 1 cup coarsely chopped roasted red peppers
- 5-1/2 Tbs. extra-virgin olive oil
- 2 tsp. rice wine vinegar
- 5 oz. (5 lightly packed cups) baby kale, baby arugula, or other baby greens

## Directions 

1. Rinse the quinoa under cold water and drain.
1. Line a medium rimmed baking sheet with parchment. In a 2-quart pot, bring the broth to a boil. Add the quinoa and simmer, covered, over low heat until the broth is absorbed and the quinoa is tender, about 15 minutes. 
1. Transfer the quinoa to a medium shallow bowl and spread out to cool to room temperature.
1. Put the white beans in a large bowl and mash with a potato masher or a fork. Add the eggs, minced garlic, thyme, 1 tsp. salt and 1/2 tsp. pepper and stir with a fork to combine. Add the breadcrumbs, parsley, olives, and cooled quinoa and stir to combine. Form the mixture into six 3-inch-wide patties about 1/2-inch thick, and transfer to the lined baking sheet.
1. Put the roasted red peppers, 2 Tbs. of the oil, the vinegar, the whole garlic clove, 1/4 tsp. salt and a few grinds of pepper in a blender and blend until very smooth, about 1 minute. Transfer the sauce to a small serving bowl.
1. In a heavy-duty 10-inch skillet, heat 2 Tbs. of the oil over medium heat until shimmering hot. Carefully add half of the patties to the pan and cook, flipping once, until browned on both sides, about 5 minutes total. Transfer to paper towels to drain and repeat with the second batch of quinoa cakes.
1. Serve the quinoa cakes over the greens with the red pepper sauce on the side for drizzling.
