---
title: "Thai Chicken Salad"
date: 2020-12-16T19:27:33-08:00
categories:
- Entrees
- Salad
tags: []
---

## Ingredients 

Dressing:
- 1/4 cup creamy peanut butter
- 2 tablespoons unseasoned rice vinegar
- 2 tablespoons fresh lime juice, from one lime
- 3 tablespoons vegetable oil
- 1 tablespoon soy sauce (use gluten-free if needed)
- 2 tablespoons honey 
- 2-1/2 tablespoons sugar 
- 2 garlic cloves, roughly chopped
- 1-inch square piece fresh ginger, peeled and roughly chopped
- 1 teaspoon salt 
- 1/4 teaspoon crushed red pepper flakes
- 2 tablespoons fresh cilantro leaves  

Salad:
- sauteed chicken
- lettuce
- cabbage
- red bell pepper
- green onions
- chopped peanuts
- carrots
- cucumber
- edamame
- cilantro

## Directions 

Toss in a blender. 

Serve over thin-sliced chicken sauteed, lettuce, cabbage, red bell pepper, green onions, chopped peanuts, carrots, cucumber, edamame, cilantro.