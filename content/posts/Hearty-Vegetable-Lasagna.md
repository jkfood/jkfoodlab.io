---
title: "Hearty Vegetable Lasagna"
date: 2020-12-22T14:46:09-08:00
categories:
- Entrees
- Pasta
tags:
- lasagna noodles
- mushrooms
- bell pepper
- zucchini
- mozzarella cheese
- ricotta cheese
- parmesan cheese
---

## Ingredients 

- 1/2 (16 ounce) package lasagna noodles
- 1 tablespoon vegetable oil
- 1/2 pound fresh mushrooms, sliced
- 1/4 cup and 2 Tbs chopped green bell pepper
- 1/4 cup and 2 Tbs chopped onion
- 1-1/2 cloves garlic, minced
- ½ a cubed zucchini 
- 1 (26 ounce) jar pasta sauce
- 1/2 teaspoon dried basil
- 1/2 (15 ounce) container part-skim ricotta cheese
- 2 cups shredded mozzarella cheese
- 1 egg
- 1 tsp salt
- 1/4 cup grated Parmesan cheese

## Directions 


1. Cook the lasagna noodles in a large pot of boiling water for 10 minutes, or until al dente. Rinse with cold water, and drain.
1. In a large saucepan, cook and stir mushrooms, green peppers, onion, and garlic in oil. Stir in pasta sauce and basil; bring to a boil. Reduce heat, and simmer 15 minutes.
1. Mix together lightly beaten egg, ricotta, 1 cup mozzarella cheese, salt, and spinich.
1. Preheat oven to 350 degrees F (175 degrees C). Spread 1/2 cup tomato sauce into the bottom of a greased 8x8 inch baking dish. Layer 1/2 each, lasagna noodles, ricotta mix, sauce, and Parmesan cheese. Repeat layering, and top with remaining 1 cup mozzarella cheese.
1. Bake, uncovered, for 40 minutes. Let stand 15 minutes before serving.
